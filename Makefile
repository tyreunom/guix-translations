# GNU Guix --- Functional package management for GNU
# Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
#
# This file is part of GNU Guix.
#
# GNU Guix is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# GNU Guix is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

GUIX:=guix-git
GUIX_WEBSITE:=guix-web-git

# These options get passed to xgettext.  We want to catch standard
# gettext uses, and SRFI-35 error condition messages.  In C++ code
# we use 'n_' instead of the more usual 'N_' for no-ops.
XGETTEXT_OPTIONS =				\
  --from-code=UTF-8				\
  --keyword=G_ --keyword=N_:1,2			\
  --keyword=message				\
  --keyword=description				\
  --keyword=n_

POTFILES:=po/doc/guix-manual.pot po/doc/guix-cookbook.pot \
		po/guix/guix.pot po/packages/packages.pot \
		website/po/guix-website.pot

define guix_version_command
$(cd $(GUIX); git log --oneline -1 | cut -f1 -d" ")
endef

define guix_web_version_command
$(cd $(GUIX_WEBSITE); git log --oneline -1 | cut -f1 -d" ")
endef

all: $(POTFILES)
	@echo "Done."

update-git: update-guix-git update-website-git

update-guix-git:
	if [ -e $(GUIX) ]; then \
		cd $(GUIX); git pull --ff-only; \
	else \
	  git clone https://git.savannah.gnu.org/git/guix.git $(GUIX); \
	fi

update-website-git:
	if [ -e $(GUIX_WEBSITE) ]; then \
		cd $(GUIX_WEBSITE); git pull --ff-only; \
	else \
	  git clone https://git.savannah.gnu.org/git/guix/guix-artwork.git $(GUIX_WEBSITE); \
	fi

#po/guix/guix.pot: po/guix.pot
#	mv $< $@
#po/packages/packages.pot: po/packages.pot
#	mv $< $@

COPYRIGHT="the authors of Guix (msgids) and the following authors (msgstr)"

GUIX_TRANSLATABLE_FILES=$(shell grep -v '^#' $(GUIX)/po/guix/POTFILES.in)
po/guix/guix.pot: $(GUIX)/po/guix/POTFILES.in $(foreach FILE,$(GUIX_TRANSLATABLE_FILES),$(GUIX)/$(FILE))
	xgettext --default-domain=$$(basename $@ | sed 's|.pot$$||') \
		     $(XGETTEXT_OPTIONS) \
	         --directory="$(GUIX)" --add-comments=TRANSLATORS: \
			 --files-from="$<" \
			 --copyright-holder=$(COPYRIGHT) \
			 --from-code=UTF-8 \
			 --keyword=G_ \
			 --keyword=N_:1,2 \
			 --keyword=message \
			 --keyword=description \
			 --keyword=synopsis \
			 --keyword=n_ \
			 --package-name="GNU guix" \
			 --package-version=$(guix_version_command) \
			 --msgid-bugs-address=bug-guix@gnu.org
	mv $$(basename $@ | sed 's|.pot$$||').po $@

PACKAGES_TRANSLATABLE_FILES=$(shell grep -v '^#' $(GUIX)/po/packages/POTFILES.in)
po/packages/packages.pot: $(GUIX)/po/packages/POTFILES.in $(foreach FILE,$(PACKAGES_TRANSLATABLE_FILES),$(GUIX)/$(FILE))
	xgettext --default-domain=$$(basename $@ | sed 's|.pot$$||') \
		     $(XGETTEXT_OPTIONS) \
	         --directory="$(GUIX)" \
			 --add-comments=TRANSLATORS: \
			 --language=Scheme \
			 --from-code=UTF-8 \
			 --keyword=synopsis \
			 --keyword=description \
			 --keyword=output-synopsis:2 \
			 --files-from="$<" \
			 --copyright-holder=$(COPYRIGHT) \
			 --from-code=UTF-8 --keyword=G_ --keyword=N_:1,2 --keyword=message \
			 --keyword=description --keyword=synopsis --keyword=n_ \
			 --package-name="GNU guix" \
			 --package-version=$(guix_version_command) \
			 --msgid-bugs-address=bug-guix@gnu.org
	mv $$(basename $@ | sed 's|.pot$$||').po $@

po/doc/%.pot: $(GUIX)/doc/%.texi
	po4a-updatepo -M UTF-8 -f texinfo -m "$<" -p "$@" \
	    --package-name "guix manual" \
		--package-version checkout \
		--copyright-holder=$(COPYRIGHT) \
		--msgid-bugs-address "bug-guix@gnu.org"

po/doc/guix-manual.pot: po/doc/contributing.pot po/doc/guix.pot
	msgcat $^ > $@

WEBSITE_POTFILES=$(GUIX_WEBSITE)/website/po/POTFILES
WEBSITE_TRANSLATABLE_FILES=$(shell grep -v '^#' $(WEBSITE_POTFILES))
website/po/guix-website.pot: $(foreach FILE,$(WEBSITE_TRANSLATABLE_FILES),$(GUIX_WEBSITE)/website/$(FILE))
	cd $(GUIX_WEBSITE)/website;					\
	guile scripts/sexp-xgettext.scm					\
	      -f po/POTFILES						\
	      -o ../../$@						\
	      --from-code=UTF-8						\
	      --copyright-holder="the authors of the Guix Website (msgid) and the following authors (msgstr)" \
	      --package-name="guix-website" 				\
	      --package-version="$$(date +%Y%m%d)"			\
	      --msgid-bugs-address="bug-guix@gnu.org"			\
	      --keyword=G_						\
	      --keyword=N_:1,2						\
	      --keyword=C_:1c,2						\
	      --keyword=NC_:1c,2,3
